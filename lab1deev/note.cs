﻿/*Итак, я пошел нелегким путем, заодно стараясь разобраться во всех граблях, на которые придется наступить, т.к. я решил описать заметку "в лоб" через поля класса.
 * Все данные, содержащиеся в заметке представляют собой отдельные поля, каждое соответственного типа (номер телефона ulong, т.к. классическая запись в 11 цифр
 * вмещается только в него, дата также обозначена через DateTime).
 * 
 * В дальнейшем я понял, что это было не лучшим решением, однако мне стало интересно, смогу ли я все таки обойтись без, к примеру, словаря для формулировки 1 заметки.
 * 
 * В этом классе все примитивно. Прошу вашего внимания в класс Interface.cs.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace NotebookAppDeevDV
{
    public class Note           // osnovnaya model dannyh prilojeniya 
    {
        public string lastname;                    //                                ! must have
        public string name;                        //                                ! must have
        public string patronymic; /*otchestvo*/
        public ulong mobileNumber;                 //only numbers                    ! must have
        public string country;                     //                                ! must have
        public DateTime birthDate;                 // ToShortDateString()
        public string organization;                //
        public string position;                    //dolzhnost
        public string note;                        //zametki
    }
}
