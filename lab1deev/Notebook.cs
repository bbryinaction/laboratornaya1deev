﻿/* Добро пожаловать в мое первое "многострочное" консольное приложение, написанное на С#. 
 * Сразу предупрежу, что комментарии будут неотъемлимой частью этого приложения.
 * Вашему вниманию предстоит 4 версия (т.е. основываясь на моем текущем опыте - максимум, который получилось извлечь) приложения записной книги.
 * Данный класс является главным, в нем же находится метод Main. Также в этом проекте есть класс заметок (Note), класс интерфейса с одноименным названием.
 * Приглашаю сперва перейти в класс Note.cs и ознакомиться с ним.
 * 
 * 
 * Итак, если вы сделали все по инструкции, то мы можем приступить к описанию "мозгов" нашего приложения.
 * Описание буду вести прямо по коду программы ниже.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace NotebookAppDeevDV
{
    class Notebook
    {
        static void Main(string[] args)
        {
            Dictionary<int, Note> StorageNote = new Dictionary<int, Note>(); // Первое - создание хранилища пары {айди, заметка}
            int uniqueID = 0;                                                // Счетчик ID

            Interface.Greeting();                                            // Инициализация интерфейса и первый вход в меню.
            Interface.Menu();
            Interface.CatchKey(ref uniqueID, ref StorageNote);
        }

        public static void CreateNote(ref int ID, ref Dictionary<int, Note> dictionary) // Самый длинный (из за того, как я описал класс Note) метод создания заметки.
        {
            string input;                                   // Переменная, которая будет содержать в себе каждое введенное пользователем значение по ходу наполнения заметки информацией.
            Note note = new Note();                         // Создание первого объекта в нашей записной книжке
                                                            // То, о чем я уже писал. Т.к. из за различных типов данных и того, что класс Note описан через множество
            bool control = true;                            // полей, я не придумал красивого цикла для наполнения заметки информацией, поэтому буду вписывать каждое поле руками.
            if (control == true)                            // Переменная control наш индикатор того, что поле введено правильно. Как только какое - то поле введено неправильно - процесс начинается сначала.
            {
                Console.Write("*Enter the lastname: ");     // Часто встречаемая структура в дальнейшем. Если в обязательном поле пользователь вводит пустую строку - прерываем создание заметки.
                input = Console.ReadLine();                  
                if (input == "")
                {
                    control = false;
                }
                else
                {
                    note.lastname = input;
                }
            }

            if (control == true)
            {
                Console.Write("*Enter the name: ");
                input = Console.ReadLine();
                if (input == "")
                {
                    control = false;
                }
                else
                {
                    note.name = input;
                }
            }

            if (control == true)
            {
                Console.Write("Enter the patronymic: ");    // Пользователь может ввести все, что захочет.
                note.patronymic = Console.ReadLine();
            }

            if (control == true)
            {
                Console.Write("*Enter the mobile number: ");    // При вводе мобильного номера сперва проверим не ввел ли он пустое поле (т.к. номер обязателен для ввода)
                input = Console.ReadLine();
                if (input == "")
                {
                    control = false;
                }
                else if (ulong.TryParse(input, out note.mobileNumber) == true)  // Если нет - проверим ввел ли он именно цифры. Если это так - заполняем поле, иначе - отмена.
                {
                    note.mobileNumber = ulong.Parse(input);
                }
                else
                {
                    control = false;
                }
            }

            if (control == true)
            {
                Console.Write("*Enter the country name: "); 
                input = Console.ReadLine();
                if (input == "")
                {
                    control = false;
                }
                else
                {
                    note.country = input;
                }
            }

            if (control == true)
            {
                Console.Write("Enter the birthdate (dd/mm/yyyy): ");        // Так же, как и выше проверяем ввел ли он именно дату. 
                input = Console.ReadLine();                                 // Если это не дата, но пустое поле - ничего не делаем, оставим значение даты по умолчанию.
                if (DateTime.TryParse(input, out note.birthDate) == true)   // Иначе - отмена. (как можно догадаться раньше везде в этой ветви использовался метод 
                {                                                           // TracebackControl. Но он, не давал выйти из создания записи и, хоть и начинался ввод 
                    note.birthDate = DateTime.Parse(input);                 // новой записи, когда нибудь после, в процессе пользования приложением оно все равно 
                }                                                           // возвращалось в эту ветку и продолжало спрашивать ввод.
                else if (input == "") { }
                else
                {
                    control = false;
                }
            }

            if (control == true)                                            // если все поля выше заполнены правильно - заполняем остальное (чем хотим)
            {
                Console.Write("Enter the organization name: ");             // а также добавляем в StorageNote пару {айди, заметка}
                note.organization = Console.ReadLine();
                Console.Write("Enter the position: ");
                note.position = Console.ReadLine();
                Console.Write("Enter the other notes: ");
                note.note = Console.ReadLine();
                Console.Clear();
                Console.WriteLine("Note was succesfully created!");

                dictionary.Add(ID, note);
                ID++;
            }
                
            if (control == false)                                           // Если какое - то из полей заполнено неправильно - переходим в метод TracebackControl
            {
                Interface.TracebackControl(ref ID, ref dictionary);
            }
        }
        public static void EditeNote(ref int ID, ref Dictionary<int, Note> dictionary)  // Сразу признаюсь что здесь нет исправления заметки, а есть удаление одной и подмена ее на новую с таким же айди
        {                                                                               // Потому что в моих условиях это должен быть еще один switch - case, где я так же
            Console.Clear();                //Руками буду отдельно перебирать каждое поле, которое захочет изменить пользователь и опять же писать все те же проверки 
            if (dictionary.Count == 0)      // (для номера телефона, даты и обязательных полей). Я уже понимаю что это займет очень много строк и принесет мало пользы.
            {
                Console.WriteLine("This notebook contains no entries."); //Проверка на наличие хотя бы одной записи в записной книжке (в дальнейшем повторяется в других методах)
            }
            else
            {
                Console.Write("Please enter the ID of an editing note: ");
                try
                {
                    int input = int.Parse(Console.ReadLine());
                    dictionary.Remove(input);                       //Удаление заметки, которую хочет редактировать пользователь
                    CreateNote(ref input, ref dictionary);          // Создание на ее месте и с ее айди новой.
                }
                catch
                {
                    Interface.TracebackControl(ref ID, ref dictionary);
                }
            }
        }
        public static void DeleteNote(ref int ID, ref Dictionary<int, Note> dictionary) //Простое удаление заметки по значению ее ключа из словаря хранилища.
        {
            Console.Clear();
            if (dictionary.Count == 0)
            {
                Console.WriteLine("This notebook contains no entries.");
            }
            else
            {
                Console.Write("Please enter the ID of a deleting note: ");
                try
                {
                    int input = int.Parse(Console.ReadLine());
                    dictionary.Remove(input);
                }
                catch
                {
                    Interface.TracebackControl(ref ID, ref dictionary);
                }
            } 
        }

        public static void OpenNote(ref int ID, ref Dictionary<int, Note> dictionary) // Открытие заметки со всеми ее полями по ее ключу.
        {
            Console.Clear();
            if (dictionary.Count == 0)
            {
                Console.WriteLine("This notebook contains no entries.");
            }
            else
            {
                Console.Write("Please enter the ID of an opening note: ");
                try
                {
                    int input = int.Parse(Console.ReadLine());
                    Console.WriteLine("Lastname: {0}", dictionary[input].lastname);
                    Console.WriteLine("Name: {0}", dictionary[input].name);
                    Console.WriteLine("Patronymic: {0}", dictionary[input].patronymic);
                    Console.WriteLine("Mobile number: {0}", dictionary[input].mobileNumber);
                    Console.WriteLine("Country: {0}", dictionary[input].country);
                    Console.WriteLine("Birth Date: {0}", dictionary[input].birthDate);
                    Console.WriteLine("Organization: {0}", dictionary[input].organization);
                    Console.WriteLine("Position: {0}", dictionary[input].position);
                    Console.WriteLine("Note: {0}", dictionary[input].note);
                }
                catch
                {
                    Interface.TracebackControl(ref ID, ref dictionary);
                }
            }
                
        }

        public static void ShowAllNote(ref Dictionary<int, Note> dictionary) // Вывод всех заметок с ключевыми полями.
        {
            Console.Clear();
            if (dictionary.Count == 0)
            {
                Console.WriteLine("This notebook contains no entries.");
            }
            else
            {
                foreach (var el in dictionary)
                {
                    Console.WriteLine("Id: " + el.Key);
                    Console.WriteLine("Lastname: " + el.Value.lastname);
                    Console.WriteLine("Name: " + el.Value.name);
                    Console.WriteLine("Telephone number: " + el.Value.mobileNumber);
                    Console.WriteLine("---");
                }
            }
        }
    }
}

/* Результатом этой лабораторной работы является не самая лучшая с точки зрения "мозгов" программа, однако в процессе проведения такого "эксперимента" я 
 точно хорошо попрактиковался, потому что она явно заняла у меня больше времени, чем у среднестатистического программиста, т.к. за те +-12-14 часов ее 
 написания я изучил многие тонкости и наступил на множество граблей =)
 К сожалению, переделать ее в "как хорошо" к дедлайну я не успею, да и, пожалуй, сохраню авторский стиль. Не хочу копировать чьи то идеи, т.к. это то, что пришло 
 мне в голову с текущим опытом и текущими знаниями. Теперь уже я вижу все + и - и, случись мне писать что - то похожее, я однозначно пойду другим путем.
 Сохраню все рассуждения на потом, спустя долгое время практики и изучения C# думаю полезно будет перечитать эту лабораторную работу.*/