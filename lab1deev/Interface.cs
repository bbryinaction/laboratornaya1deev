﻿/* Итак, мне очень хотелось отделить "мозги" от визуальной части этого приложения, поэтому я и создал этот класс. Заодно лучше разобрался с тем, какие значения
 * передаются в вызываемые, из других классов, методы и пр.
 * 
 * К вашему вниманию метод Greeting. Простое окно приветствия. Метод Menu - также простой вариант символьной отрисовки меню.
 * Метод CatchKey. На мой взгляд лучшее из пришедших на ум, в плане простоты и действенности, описание навигации через рекурсию.
 * 
 * В ответ на нажатую клавишу (catch), метод вызывает соответствующий метод из класса с "мозгами" (создание, удаление и пр.), 
 * потом опять отрисовывает меню и снова вызывает сам себя для дальнейшего взаимодействия (само собой после завершения выполнения вышесказанного метода).
 * 
 * Метод TracebackControl. В предыдущих версиях он встречался чаще по коду программы, однако в дальнейшем заменен костылем, т.к. не позволял выйти из метода создания
 * заметки, в случае своего срабатывания (об этом позже). Он используется в обработке исключений и возвращает нас обратно в меню.
 * 
 * Прошу вашего внимания снова таки в главный класс Notebook.cs.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace NotebookAppDeevDV
{
    public class Interface
    {
        private static string literal = "alpha";
        private static double version = 0.4;
        public static void Greeting()
        {
            Console.WriteLine("Welcome to the Notebook. Current version is: {0}-{1}.", literal, version);
            Console.WriteLine("To control the application you need to press the appropriate buttons.");
        }
        public static void Menu()
        {
            Console.WriteLine("========================");
            Console.WriteLine("|| Create new note  C ||");
            Console.WriteLine("|| Edit note        E ||");
            Console.WriteLine("|| Delete note      D ||");
            Console.WriteLine("|| Open a note      O ||");
            Console.WriteLine("|| Show all notes   S ||");
            Console.WriteLine("|| Exit             Z ||");
            Console.WriteLine("========================");
        }
        public static void CatchKey(ref int ID, ref Dictionary<int, Note> dictionary)
        {
            Console.Write("Press any key: ");
            switch (Console.ReadKey().Key)
            {
                case ConsoleKey.C: //creating new note
                    Console.Clear();
                    Console.WriteLine("New note was created. Please load the information");
                    Notebook.CreateNote(ref ID, ref dictionary); 
                    Menu();
                    CatchKey(ref ID, ref dictionary);
                    break;

                case ConsoleKey.E: //editing created note
                    Notebook.EditeNote(ref ID, ref dictionary);
                    Menu();
                    CatchKey(ref ID, ref dictionary);
                    break;

                case ConsoleKey.D: //deliting created note
                    Notebook.DeleteNote(ref ID, ref dictionary);
                    Menu();
                    CatchKey(ref ID, ref dictionary);
                    break;

                case ConsoleKey.O: //opening created note
                    Notebook.OpenNote(ref ID, ref dictionary);
                    Menu();
                    CatchKey(ref ID, ref dictionary);
                    break;

                case ConsoleKey.S: //showing all notes
                    Notebook.ShowAllNote(ref dictionary);
                    Menu();
                    CatchKey(ref ID, ref dictionary);
                    break;

                case ConsoleKey.Z: //exit program
                    break;
                default:
                    Console.WriteLine("\n"+"Please try again!");
                    CatchKey(ref ID, ref dictionary);
                    break;
            }
        }

        public static void TracebackControl(ref int ID, ref Dictionary<int, Note> dictionary)
        {
            Console.Clear();
            Console.WriteLine("Oops! Smth wrong was happened! Try again =)");
            Menu();
            CatchKey(ref ID, ref dictionary);
        }
    }
}
